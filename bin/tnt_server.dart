import 'package:tiny_http/tiny_http.dart';
import 'package:tnt_server/tnt_app_channel.dart';
void main(List<String> arguments) async{
  print('tnt_server starting...');
  var server = Server<TntAppChannel>('localhost', 8888);
  await server.start();
  print('tnt_server started...');
}

