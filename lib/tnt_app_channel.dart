import 'package:tiny_http/tiny_http.dart';
import 'package:tiny_orm/tiny_orm.dart';
import 'package:tnt_server/config/Configuration.dart';
import 'package:tnt_server/endpoint_controllers/CourseController.dart';

class TntAppChannel implements ApplicationChannel {
  static const String getMethod = 'GET';
  static const String posMethod = 'POST';
  static const String updateMethod = 'UPDATE';
  static const String deleteMethod = 'DELETE';
  static const String courseEndpoint = '/course';
  static const String coursesEndpoint = '/courses';
  late Connection connection;

  @override
  void prepare() {
    connection = Connection(ServerConfiguration.database_host,
        ServerConfiguration.database_port, ServerConfiguration.database_dbname,
        username: ServerConfiguration.database_username,
        password: ServerConfiguration.database_password);
  }

  @override
  Router getEndpoint() {
    var router = Router();
    router.route(courseEndpoint).link(() => CourseController(connection));
    return router;
  }

  @override
  void finalise() {}
}

