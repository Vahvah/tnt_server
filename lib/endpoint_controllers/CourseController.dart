import 'package:tiny_http/tiny_http.dart';
import 'package:tnt_server/tnt_app_channel.dart';
import 'package:tiny_orm/tiny_orm.dart';

class CourseController extends Controller {
  final Connection connection;

  CourseController(this.connection);

  @override
  Future<RequestOrResponse> handle(Request request) async {
    switch (request.method) {
      case TntAppChannel.getMethod:
        return get(request);
      case TntAppChannel.updateMethod:
        return update(request);
      case TntAppChannel.deleteMethod:
        return delete(request);
      case TntAppChannel.posMethod:
        return post(request);
    }
    return request;
  }

  RequestOrResponse get(Request request) {
    return request;
  }

  RequestOrResponse update(Request request) {
    return request;
  }

  RequestOrResponse post(Request request) {
    return request;
  }

  RequestOrResponse delete(Request request) {
    return request;
  }
}
